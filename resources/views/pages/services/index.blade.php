<?php
$stylesheet = "services";
?>
@extends('layouts.app')

@section('content')
    <div class="topBanner">
        <div class="cover">
            <div class="inner container">
                <h3 class="animated fadeIn">Services</h3>
                <div class="divide"></div>
                <h4>Learn who we are, and what we do</h4>
            </div>
        </div>
    </div>
    <div class="bottomArea clearfix">
        <div class="innerBottomArea clearfix">
            <div class="navigationServices" style="display: none;">

            </div>
            <div class="firstSection service clearfix webDevelopment">
                <div class="mainContent">
                    <div class="inner container">
                        <div class="topTitle">
                            <h3>Web Development</h3>
                            <h4 class="sub">This is what we're made for!</h4>
                        </div>
                        <div class="bottomContent">
                            <p>This is the heart of our company, and it's what we specialize in! We take pride in our websites, and we aim to give you the best looking website for you company at an affordable rate! We work fast to plan, build and launch your website so you can quickly start to gain more customers. We always give our best and we never take shortcuts, everything is done with love.</p>
                            <div class="subSection subOne">
                                <h3>What do you get?</h3>
                                <ul>
                                    <li>1. You get a custom made website that represents who you are and what you do.</li>
                                    <li>2. No pre-made templates or cheap themes. We make everything from scratch, and promise to build something that's creative, and made with love!</li>
                                    <li>3. All of our websites are built for all screen sizes. Meaning your website will look great on mobile phones, tablets and desktops!</li>
                                    <li>4. We make use of your photos, videos and any text you provide to us. We will place them in the correct spots and highlight the most meaningful things</li>
                                </ul>
                            </div>
                            <div class="subSection subTwo">
                                <h3>Whats the process?</h3>
                                <ul>
                                    <li>1. Once you <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=Lets start our journey">contact us</a>, we will plan the website together, and come up with an initial estimate</li>
                                    <li>2. You will pay 1/3 of the initial estimate, and then thats when development will start!</li>
                                    <li>3. One the website is completed to your satisfaction, we will bill the final 2/3 of the initial estimate.</li>
                                    <li>4. Once the last 2/3 of the initial estimate is paid, we will hand over ownership to you.</li>
                                </ul>
                            </div>
                            <div class="subSection subThree">
                                <h3>Then what?</h3>
                                <p>Our journey does not end there! We will provide maintenance, support and SEO help at a discounted monthly rate! We value long-term relationships and we want to be able to further your company! </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="secondSection service clearfix webMaintenance">
                <div class="mainContent">
                    <div class="inner container">
                        <div class="topTitle">
                            <h3>Web Maintenance</h3>
                            <h4 class="sub">Already got a site but need things done with it?</h4>
                        </div>
                        <div class="bottomContent">
                            <p>This another one of our main services that we provide to our clients! What do you do when you already have a website but it's either outdated or has alot of problems? Well we have the answer to that! We provide stellar web maintenance services to our clients at a very affordable monthly rate. Depending on the size of your company, we can help you pick the right package for you that will suit your websites needs.</p>
                            <div class="subSection subOne">
                                <h3>Plans</h3>
                                <div class="plansHold clearfix">
                                    <div class="plan col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="innerPlan">
                                            <div class="topTitle">
                                                <h3>Entry Support</h3>
                                                <h4 class="sub">(80 Hours)</h4>
                                            </div>
                                            <div class="middleContent hidden-sm">
                                                <ul>
                                                    <li>Best for Tiny Businesses</li>
                                                    <li class="middle bold">80 Hours (No rollover)</li>
                                                    <li>24/7 Support</li>
                                                </ul>
                                            </div>
                                            <div class="bottomSection">
                                                <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=About your maintenance plans (Entry)">
                                                    <div class="planPriceButton">
                                                        <h3>$210</h3>
                                                        <h4>Monthly</h4>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="plan col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="innerPlan">
                                            <div class="topTitle">
                                                <h3>Standard Support</h3>
                                                <h4 class="sub">(10 Hours)</h4>
                                            </div>
                                            <div class="middleContent hidden-sm">
                                                <ul>
                                                    <li>Best for small businesses</li>
                                                    <li class="middle bold">150 Hours (No rollover)</li>
                                                    <li>24/7 Support</li>
                                                </ul>
                                            </div>
                                            <div class="bottomSection">
                                                <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=About your maintenance plans (Standard)">
                                                    <div class="planPriceButton">
                                                        <h3>$420</h3>
                                                        <h4>Monthly</h4>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="plan col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="innerPlan">
                                            <div class="topTitle">
                                                <h3>Premier Support</h3>
                                                <h4 class="sub">(20 Hours)</h4>
                                            </div>
                                            <div class="middleContent hidden-sm">
                                                <ul>
                                                    <li>Small to medium businesses</li>
                                                    <li class="middle bold">250 Hours (No rollover)</li>
                                                    <li>24/7 Support</li>
                                                </ul>
                                            </div>
                                            <div class="bottomSection">
                                                <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=About your maintenance plans (Premier)">
                                                    <div class="planPriceButton">
                                                        <h3>$650</h3>
                                                        <h4>Monthly</h4>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="subSection subTwo">
                                <h3>How does this work</h3>
                                <ul>
                                    <li>1. Once we come to an agreement on what you want and need, we will bill you for your first month!</li>
                                    <li>2. You will then send us a list of things that you want changed on your website, or any bugs you may have!</li>
                                    <li>3. Once we get that from you, then we will proceed to do the work that's requested!</li>
                                    <li>4. And thats all you need to do! We will take care of the rest</li>
                                </ul>
                            </div>
                            <div class="subSection subTwo">
                                <h3>Lets Start!</h3>
                                <p>Ready to start this new journey? <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=About your maintenance plans">Contact us</a> now and we will be in touch</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="combinedSections">
                <div class="otherServices">
                    <h3>Other services</h3>
                </div>
                <div class="leftSection service col-lg-6 col-xs-12">
                    <div class="mainContent">
                        <div class="inner">
                            <div class="topTitle">
                                <h3>Seo Optimization</h3>
                                <h4 class="sub">Need your website listed on search engines?</h4>
                            </div>
                            <div class="bottomContent">
                                <p>You've gotten your finished website, it's launched and ready to go. But how will people find your website? If they searched your website in Google or Yahoo, what would show up? That's where we come in! We will find ways to quickly boost your SEO rankings, and also get your website out there for people to find you. We will make your website viewable on all the popular search engine platforms out there and we will spot common mistakes on your website that causes your website to rank low.</p>
                                <div class="subSection subOne">
                                    <h3>How does it work?</h3>
                                    <p>This is a monthly service so you will pay monthly for us to keep your website ranked high and give you meaningful advice on how to keep your website ranked highly on all the biggest search engine giants</p>
                                    <p><b>Price: $50/month</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rightSection service col-lg-6 col-xs-12">
                    <div class="mainContent">
                        <div class="inner">
                            <div class="topTitle">
                                <h3>Social Media Marketing</h3>
                                <h4 class="sub">Trying to boost your social media following?</h4>
                            </div>
                            <div class="bottomContent">
                                <p>Got to much on your to-do list to worry about all of your social media accounts? Well we're here to help with that and can take care of all of your social media needs.</p>
                                <div class="subSection subOne">
                                    <h3>What you'll get</h3>
                                    <p>We will take care of your social media accounts and manage them for you! We know that you're busy, that's why we're here to help you and take the stress off of you regarding your social media accounts. We will also help advertise and get you more followers on your account!</p>
                                </div>
                                <div class="subSection subTwo">
                                    <h3>And how does this work?</h3>
                                    <p>You pay a small monthly fee, and we will do all the work for you. Tell us what you want us to post, edit or delete from your accounts and we will do it. Plus we will actively work to get you more followers and more customers through your social media accounts</p>
                                    <p><b>Price: $50/month</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection