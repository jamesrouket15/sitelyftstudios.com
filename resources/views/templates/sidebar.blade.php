<div class="sidebarMain">
    <div class="innerSidebar">
        <div class="topHead">
            <img src="<?php echo url("/"); ?>/images/sitelyft-circle-logo-CIRCLE.png" />
            <h3>Menu</h3>
        </div>
        <div class="bottomMenu">
            <ul itemscope='itemscope' itemtype='http://schema.org/SiteNavigationElement' role='navigation'>
                <li itemprop='url' title="Learn who we are and what we do"><a itemprop='name' href="<?php echo url("/"); ?>/about">About Us</a></li>
                <li itemprop='url' title="Learn about our services that we provide"><a itemprop='name' href="<?php echo url("/"); ?>/services">Our Services</a></li>
                <li><a href="<?php echo url("/"); ?>#work">Our Work</a></li>
                <li class="special"><a class="emailContactPoint" href="mailto:james@sitelyftstudios.com?subject=Lets start our journey">Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>