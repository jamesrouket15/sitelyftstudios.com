<header class="mainHeader">
    <div class="innerMainHeader container">
        <div class="leftBranding clearfix col-lg-7 col-md-6 col-sm-3 col-xs-3">
            <a href="<?php echo url("/"); ?>">
                <img src="<?php echo url("/"); ?>/images/sitelyft-circle-logo-CIRCLE.png" />
                <h3>Sitelyft Studios</h3>
            </a>
        </div>
        <div class="rightMenu col-lg-5 col-md-6 col-sm-9 col-xs-9">
            <div class="innerMenu">
                <ul itemscope='itemscope' itemtype='http://schema.org/SiteNavigationElement' role='navigation'>
                    <div class="mobileHide">
                        <li itemprop='url' title="Learn who we are and what we do"><a itemprop='name' href="<?php echo url("/"); ?>/about">About Us</a></li>
                        <li itemprop='url' title="Learn about our services that we provide"><a itemprop='name' href="<?php echo url("/"); ?>/services">Services</a></li>
                    </div>
                    <li class="special"><a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=Lets start our journey">Contact Us</a></li>
                    <li class="mobileSidebarOpen"><i class="fa fa-bars" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
    </div>
</header>
