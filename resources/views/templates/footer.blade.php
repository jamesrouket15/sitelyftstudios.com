<div class="bottomWebsite">
    <div class="preFooter">
        <div class="innerFooter">
            <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=Lets start our journey">Start a project with us!</a>
        </div>
    </div>
    <div class="footer">
        <div class="topMainFooterArea container">
            <div class="leftMainInfo col-lg-6">
                <div class="topArea">
                    <h3>Lets start our journey together!</h3>
                </div>
                <div class="bottomArea">
                    <ul>
                        <li>Email: <a class="emailContactPoint" href="mailto:hello@sitelyftstudios.com?subject=Lets start our journey">hello@sitelyftstudios.com</a></li>
                        <li>Phone: <a class="phoneContactPoint" href="tel:14404531380">1-440-453-1380</a></li>
                        <br />
                        <li>Lorain, Ohio</li>
                        <li>United States</li>
                    </ul>
                </div>
            </div>
            <div class="rightMainMenu col-lg-6">

            </div>
        </div>
        <div class="bottomCopyright container">
            <div class="leftCopyright col-lg-6 col-md-6 col-sm-6">
                <h3>Copyright 20<?php echo date('y'); ?> - Sitelyft Studios</h3>
            </div>
            <div class="rightCopyright col-lg-6 col-md-6 col-sm-6">
                <h3>Courtesy of the <a href="https://sitelyftstudios.com">mob</a></h3>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="<?php echo url('/'); ?>/js/jquery.js"></script>
<script src="<?php echo url('/'); ?>/js/main.js"></script>

<!-- Google Structured Data -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "https://sitelyftstudios.com",
  "logo": "https://sitelyftstudios.com/images/sitelyft-circle-logo-CIRCLE.png",
  "name": "Sitelyft Studios, LLC",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+4404531380",
    "contactType": "Customer service"
  },
  "sameAs": [
    "https://twitter.com/sitelyftstudios",
    "https://www.facebook.com/sitelyftstudios/",
    "https://www.linkedin.com/in/jameslattenjr/"
  ]
}
</script>
</body>
</html>